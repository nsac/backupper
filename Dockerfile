FROM alpine:latest

LABEL maintainer="Richard Brinkman <server@nsac.alpenclub.nl>"

ENV GIT_USER_NAME="NSAC VPS" \
    GIT_USER_EMAIL="server@nsac.alpenclub.nl"

RUN apk --no-cache add \
      git \
      mariadb-client \
      openssh-client \
      openssh-keygen

COPY ssh.conf /root/.ssh/config
RUN chmod 600 /root/.ssh/config

VOLUME ["/backup/.git", "/root/.ssh"]

COPY entry.sh /

ENTRYPOINT ["/entry.sh"]
