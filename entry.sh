#!/bin/sh

if [ ! -f /root/.ssh/id_rsa.pub ]; then
	echo "This is the first time you run the backupper. I'll will not backup anything yet, but will generate a public/private keypair instead."
	ssh-keygen -f /root/.ssh/id_rsa -N ""
	echo Copy / paste the following RSA public key to the Gitlab repository and rerun the backupper to start the backup:
	cat /root/.ssh/id_rsa.pub
	exit 0
fi

[ -z "${GIT_URL}" ] && echo You should set the GIT_URL variable to the ssh repository && exit 1

git config --global user.email "${GIT_USER_EMAIL:-server@nsac.alpenclub.nl}"
git config --global user.name "${GIT_USER_NAME:-NSAC automatic backupper}"

cd /backup

if test -n "${MYSQL_DATABASE}" -a -n "${MYSQL_USER}" -a -n "${MYSQL_PASSWORD}" -a -n "${MYSQL_HOST}"; then
	echo Dumping database $MYSQL_DATABASE
	mysqldump --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQLDUMP_PARAMETERS} ${MYSQL_DATABASE} | sed -e 's/),(/),\n(/g' > ${MYSQL_DATABASE}.sql
fi

[ ! -f .git/HEAD ] && git init && git config pack.windowmemory 500m && git config gc.autoDetach false
git remote get-url origin || git remote add origin ${GIT_URL}
git fetch origin
git status | grep -q "Your branch and 'origin/master' have diverged" && git reset --soft origin/master
git status | grep -qE "Your branch is behind 'origin/master' by [0-9]+ commits?, and can be fast-forwarded." && git reset --soft origin/master

if [ `date +%d` -eq 01 ]; then
	git repack -d
	git push origin master
	git branch --move --force previousmonth
	git push --force origin previousmonth
	git checkout --orphan master
fi

git add .
git commit --message "Backup $(date +%Y-%m-%d)"

# only push to Gitlab if more than 100 objects
if `git count-objects -v | grep -qE 'count: [0-9]{3,}'`; then
	git repack -d
	git push --force origin master
	git gc --auto
fi
